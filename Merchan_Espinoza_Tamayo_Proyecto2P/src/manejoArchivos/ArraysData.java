package manejoArchivos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;
import modelo.CopaMundial;
import modelo.Jugador;
import modelo.Pais;
import modelo.Partido;

/**
 *
 * @author Carla
 */
public class ArraysData {

    public static ArrayList<CopaMundial> copasMundiales;
    public static ArrayList<Pais> paises;
    public static ArrayList<Jugador> jugadores;
    public static ArrayList<Partido> partidos;
    public static TreeMap<String, ArrayList<Partido>> fasesPartidos;

    public ArraysData() {
        crearObjetosPartido();
        crearObjetosCopaMundial();
        crearObjetosJugadores();
        crearMapFasesPartidos();
        
    }

    /**
     * Metodo que permite crear un arreglo de objetos Partido y Pais
     */
    private void crearObjetosPartido() {
        //arrayPartidos
        paises = new ArrayList<>(); //Almacena obejetos de tipo Pais no REPETIDOS
        partidos = new ArrayList<>();
        ArrayList<String> lineas = ManejoArchivo.leerArchivo("WorldCupMatches.csv");
        String informacion = lineas.remove(0);

        for (String linea : lineas) {
            String[] dato = linea.split(",");
            //Creacion de objetos de tipo Pais
            Pais pais = new Pais(dato[5].trim());
            pais.setIniciales(dato[18].trim());
            pais.getFases().add(dato[2]);
            // Verifico si el objeto no se encuentra agregado en la lista paises
            if (!paises.contains(pais)) {
                paises.add(pais);
            } else {
                for (Pais p : paises) {
                    if (p.equals(pais)) {
                        p.getFases().add(dato[2]);
                    }
                }
            }
            //Creacion de objetos de tipo Partidos
            String fecha = dato[1].replace("\"", "").trim();
            String ciudad = dato[4].replace("\"", "").trim();
            Partido p = new Partido(dato[0], fecha, dato[2], dato[3], ciudad,
                    dato[5], dato[6], dato[7], dato[8], dato[10], dato[16],
                    dato[17].trim(), dato[18].trim());
            partidos.add(p);
        }
        Collections.sort(partidos);
        //System.out.println(paises);
        //System.out.println(partidos);
    }

    
    /**
     * Metodo que permite crear un arreglo de objetos Copa Mundial
     */
    private void crearObjetosCopaMundial() {
        copasMundiales = new ArrayList<>();
        ArrayList<String> lineas = ManejoArchivo.leerArchivo("WorldCups.csv");
        String informacion = lineas.remove(0);
        for (String linea : lineas) {
            String[] dato = linea.split(",");
            Pais primero = null;
            Pais segundo = null;
            Pais tercero = null;
            Pais cuarto = null;
            for (int i = 2; i < 6; i++) {//Indices donde se encuentran los nombres de paises ganadores
                Pais p = new Pais(dato[i]);//Creo el objeto                    
                if (!paises.contains(p)) {//Compruebo que no exista el objeto para a;adirlo a la lista
                    paises.add(p);
                    switch (i) { //Este bloque le asigna los valores de tipo Pais de acuerdo a las posiciones  
                        case 2:
                            primero = p;
                            break;
                        case 3:
                            segundo = p;
                            break;
                        case 4:
                            tercero = p;
                            break;
                        case 5:
                            cuarto = p;
                            break;
                    }
                } else { //En caso de que exista, se recorre la lista paises
                    for (Pais pais : paises) {
                        if (pais.equals(p)) {
                            switch (i) { //De acuerdo a las posiciones se asignan los valores de tipo Pais ya existente en la lista
                                case 2:
                                    primero = pais;
                                    break;
                                case 3:
                                    segundo = pais;
                                    break;
                                case 4:
                                    tercero = pais;
                                    break;
                                case 5:
                                    cuarto = pais;
                                    break;
                            }
                        }
                    }
                }
            }
            //Bloque que modifica el numero de copas ganadado por cada pais 
            for (Pais pais : paises) {
                if (pais.getPais().equals(dato[2])) {
                    pais.ganaCopa();
                }
            }
            //Creacion de objetos de tipo CopaMundial
            int year = Integer.parseInt(dato[0]);
            int goalsTotal = Integer.parseInt(dato[6]);
            int teams = Integer.parseInt(dato[7]);
            int matches = Integer.parseInt(dato[8]);

            CopaMundial cm = new CopaMundial(year, primero, segundo, tercero, cuarto, goalsTotal, teams, matches, dato[9]);
            copasMundiales.add(cm);
        }
        //System.out.println(copasMundiales);
    }

    /**
     * Metodo que permite crear un arreglo de objetos Jugador
     */
    public void crearObjetosJugadores() {

        jugadores = new ArrayList<>(); //Almacena obejetos de tipo Pais no REPETIDOS
        ArrayList<String> lineas = ManejoArchivo.leerArchivo("WorldCupPlayers2014.csv");
        lineas.remove(0);

        for (String linea : lineas) {
            String[] dato = linea.split(",");
            //Creacion de objetos de tipo Jugador
            int shirtNum = Integer.parseInt(dato[5]);
            Jugador jj = new Jugador(dato[6], shirtNum, dato[2],
                    dato[3], dato[1]);
            jugadores.add(jj);
        }
        Collections.sort(jugadores);
        //System.out.println(jugadores);
    }

    /**
     * Metodo que permite crear un TreeMap con claves tipo String de las fases de
     * la copa Mundial y con valores de tipo ArrayList que contienen todos los partidos
     * pertenecientes a dicha fase
     */
    public void crearMapFasesPartidos() {

        fasesPartidos = new TreeMap<>();

        for (Partido p : partidos) {

            switch (p.getFase()) {
                case "Group A":
                    llenarMapFasesPartidos("A", p);
                    break;
                case "Group B":
                    llenarMapFasesPartidos("B", p);
                    break;
                case "Group C":
                    llenarMapFasesPartidos("C", p);
                    break;
                case "Group D":
                    llenarMapFasesPartidos("D", p);
                    break;
                case "Group E":
                    llenarMapFasesPartidos("E", p);
                    break;
                case "Group F":
                    llenarMapFasesPartidos("F", p);
                    break;
                case "Group G":
                    llenarMapFasesPartidos("G", p);
                    break;
                case "Group H":
                    llenarMapFasesPartidos("H", p);
                    break;
                case "Round of 16":
                    llenarMapFasesPartidos("Ronda de 16", p);
                    break;
                case "Quarter-finals":
                    llenarMapFasesPartidos("Cuartos de Final", p);
                    break;
                case "Semi-finals":
                    llenarMapFasesPartidos("SemiFinal", p);
                    break;
                case "Play-off for third place":
                    llenarMapFasesPartidos("Play off Tercer Lugar", p);
                    break;
                case "Final":
                    llenarMapFasesPartidos("Final", p);
                    break;
                default:
                    System.out.println("No existe fase");
            }
        }
        //System.out.println(fasesPartidos);
    }

    public void llenarMapFasesPartidos(String fase, Partido p) {

        if (!fasesPartidos.containsKey(fase)) {
            fasesPartidos.put(fase, new ArrayList<>());
        } else {
            fasesPartidos.get(fase).add(p);
        }
    }

    public ArrayList<CopaMundial> getCopasMundiales() {
        return copasMundiales;
    }

    public static ArrayList<Pais> getPaises() {
        return paises;
    }

    public static TreeMap<String, ArrayList<Partido>> getFasesPartidos() {
        return fasesPartidos;
    }
}
