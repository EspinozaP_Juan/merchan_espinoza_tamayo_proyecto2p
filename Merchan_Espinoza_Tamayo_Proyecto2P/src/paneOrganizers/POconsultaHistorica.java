/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paneOrganizers;

import java.io.IOException;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import manejoArchivos.ArraysData;
import modelo.CopaMundial;

/**
 *
 * @author Carla
 */
public class POconsultaHistorica {

    private VBox root;
    private VBox seccionPremio;
    private VBox seccionDatosGenerales;
    private GridPane resultados;
    private ArrayList<CopaMundial> copasMundiales;
    //Nodos con eventos
    private Button consultar;
    private Label mensaje;
    private Button atras;
    //Nodos con informacion
    private TextField yearData;
    private Scene sc;
    private Stage primaryStage;

    public POconsultaHistorica(ArrayList<CopaMundial> copasMundiales, Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.copasMundiales = copasMundiales;
        root = new VBox();
        root.setAlignment(Pos.TOP_CENTER);
        root.setSpacing(35);
        topSection();

        //String[] posiciones = {"Ganador", "Segundo", "Tercero", "Cuarto"};
        //Se crea la seccion resultados
        resultados = new GridPane();
        resultados.setAlignment(Pos.CENTER);
        resultados.setHgap(60);
        root.getChildren().add(resultados);
        atras = new Button("Atrás");
        atras.setAlignment(Pos.BOTTOM_CENTER);

        root.getChildren().add(atras);
        //Evento que permite mostrar el año consultado a partir del boton consultar
        EventHandler<ActionEvent> e = new EventHandler() {
            @Override
            public void handle(Event event) {
                resultados.getChildren().clear();
                try {
                    filtrarResultados();
                } catch (NullPointerException e) {
                    System.out.println(e.getMessage());
                }

            }
        };
        consultar.setOnAction(e);
        sc = new Scene(root, 700, 500);

    }

    public VBox getRoot() {
        return this.root;
    }

    /**
     * Metodo que permite crear el manejo del evento para el buton atras
     * @return la Scene de la clase
     */
    public Scene getScene() {
        EventHandler<ActionEvent> s = new EventHandler() {
            @Override

            public void handle(Event event) {
                Scene scene2 = new Principal(primaryStage).getScene();
                primaryStage.setScene(scene2);
            }
        };
        atras.setOnAction(s);

        try {
            sc.getStylesheets().add(getClass().getResource("/resources/estilo_ch.css").toString());
        } catch (Exception e) {
            System.out.println("No muestra estilos");
        }

        return sc;
    }

    /**
     * Metodo que crea la parte superior de la interfaz grafica
     */
    private void topSection() {
        Label titulo = new Label("Consulta Historia de Copas \nMundiales");
        titulo.setTextAlignment(TextAlignment.CENTER);
        titulo.setPadding(new Insets(25, 5, 0, 5));
        titulo.setId("titulo");
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        hb.setSpacing(20);
        Label l1 = new Label("Año");
        yearData = new TextField();
        consultar = new Button("CONSULTAR");
        hb.getChildren().addAll(l1, yearData, consultar);
        root.getChildren().addAll(titulo, hb);
    }


    /**
     * Metodo que muestra los resultados de la copa mundial escogida segun el año
     */
    private void mostrarResultado() {
        //Seccion Premios
        Label lb = new Label("Premios");
        lb.setId("subtitulo");
        Line linea = new Line();
        linea.setEndX(350);
        resultados.add(lb, 0, 0);
        resultados.add(linea, 0, 1);
        resultados.add(seccionPremio, 0, 2);
        //Seccion Datos generales
        Line linea2 = new Line();
        linea2.setEndX(150);
        Label lb2 = new Label("Datos generales");
        lb2.setId("subtitulo");
        resultados.add(lb2, 1, 0);
        resultados.add(linea2, 1, 1);
        resultados.add(seccionDatosGenerales, 1, 2);
    }

    private void premio(String posicion, String pais, int numCopas) throws IOException {
        HBox HBpremio = new HBox();
        HBox bandera_pais = new HBox();
        bandera_pais.setSpacing(3);
        bandera_pais.setAlignment(Pos.BASELINE_LEFT);
        bandera_pais.setPrefWidth(110);

        System.out.println(posicion + " " + pais + " " + numCopas);
        Image img = new Image(getClass().getResource("/resources/BanderasCopa2014/" + pais.toUpperCase() + ".png").toString());
        ImageView paisIV = new ImageView(img);
        paisIV.setFitHeight(10);
        paisIV.setFitWidth(12);

        Label paisLabel = new Label(pais.toUpperCase());

        bandera_pais.getChildren().addAll(paisIV, paisLabel);

        HBox imgCopas = new HBox();
        imgCopas.setSpacing(10);
        for (int i = 0; i < numCopas; i++) {
            Image img2 = new Image(getClass().getResource("/resources/Copa.png").toString());
            ImageView copaIV = new ImageView(img2);
            copaIV.setFitHeight(23);
            copaIV.setPreserveRatio(true);
            imgCopas.getChildren().add(copaIV);
        }
        Label psLabel = new Label(posicion);
        psLabel.setPrefWidth(80);

        HBpremio.getChildren().addAll(psLabel, bandera_pais, imgCopas);
        HBpremio.setSpacing(30);
        seccionPremio.getChildren().add(HBpremio);
    }

    private VBox seccionPremios(String[] posiciones, String[] paises, int[] copas) {
        seccionPremio = new VBox();
        seccionPremio.setSpacing(15);
        seccionPremio.setPadding(new Insets(15, 5, 15, 5));
        try {

            for (int i = 0; i < posiciones.length; i++) {
                premio(posiciones[i], paises[i], copas[i]);
            }
        } catch (IOException | RuntimeException e) {
            seccionPremio.getChildren().clear();
            System.out.println("No se muestra seccion premios");
            seccionPremio.getChildren().add(new Label("Ha ocurrido un error"));
        }
        return seccionPremio;
    }

    private VBox seccionDatosGenerales(int goles, int equipos, int partJugados, String asistencia) {
        seccionDatosGenerales = new VBox();
        seccionDatosGenerales.setSpacing(22);
        seccionDatosGenerales.setPadding(new Insets(15, 5, 15, 5));
        Label l1 = new Label("Goles anotados: " + goles);
        Label l2 = new Label("Equipos: " + equipos);
        Label l3 = new Label("Partidos jugados: " + partJugados);
        Label l4 = new Label("Asistencia: " + asistencia);
        seccionDatosGenerales.getChildren().addAll(l1, l2, l3, l4);
        return seccionDatosGenerales;
    }

    //Verfica si existen o no el resultado pedido por el usuario
    private void filtrarResultados() {
        Boolean check = false;
        String[] posiciones = {"Ganador", "Segundo", "Tercero", "Cuarto"};
        for (CopaMundial copaMundial : copasMundiales) {
            String year = Integer.toString(copaMundial.getYear());
            if (yearData.getText().trim().equals(year)) {//Posible error porque el year : int en CopaMundial
//                String[] paises = {"Alemania", "Argentina", "Paises Bajos", "Brasil"};
                String[] paises = {copaMundial.getFirst().getPais(), copaMundial.getSecond().getPais(), copaMundial.getThird().getPais(), copaMundial.getFourth().getPais()};
                int[] numCopas = {copaMundial.getFirst().getCopas(), copaMundial.getSecond().getCopas(), copaMundial.getThird().getCopas(), copaMundial.getFourth().getCopas()};
                //int[] numCopas = {1, 2, 0, 5};
                for (String pais : paises) {
                    System.out.println(pais);
                }
                seccionPremios(posiciones, paises, numCopas);
                seccionDatosGenerales(copaMundial.getGoalsTotal(), copaMundial.getTeams(), copaMundial.getMatches(), copaMundial.getAttendance());
                //seccionDatosGenerales(171, 32, 64, 3386810);
                mostrarResultado();
                check = true;
            }
        }
        if (!check) {
//            System.out.println(yearData.getText() + " " + year);
            resultados.add(new Label("No existe registro para el año " + yearData.getText()), 0, 0);
        }

    }
}
