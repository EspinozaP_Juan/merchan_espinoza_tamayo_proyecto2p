/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paneOrganizers;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import modelo.Jugador;

/**
 *
 * @author Carla
 */
public class POdetalleJugador {
    private VBox root;
    private Label mensaje;
    private Jugador jugador;

    public POdetalleJugador(Jugador jugador) {
        this.jugador = jugador;
        root = new VBox();
        root.setSpacing(25);
        root.setAlignment(Pos.CENTER);
        Label nombre = new Label(jugador.getNombre());
        ImageView iv = new ImageView(jugador.getImg());
        iv.setPreserveRatio(true);
        iv.setFitWidth(100);
        root.getChildren().addAll(nombre,iv);
        crearDescripcion();
        mensaje = new Label("Mostrando por 10 segundos");
        root.getChildren().add(mensaje);
        
        
        
    }
    public void crearDescripcion(){
        Label l = new Label(jugador.getTeamInitials()+"\nCAMISETA NRO "+
                jugador.getShirtNum()+"\nDIR. TEC. "+jugador.getCoach()+"\n");
        l.setTextAlignment(TextAlignment.CENTER);
        l.setPadding(new Insets(10, 12, 10, 12));
        l.setAlignment(Pos.CENTER);
        l.setTextAlignment(TextAlignment.CENTER);
        root.getChildren().add(l);
    }

    public Label getMensaje() {
        return mensaje;
    }

    public VBox getRoot() {
        return root;
    }
    
}
