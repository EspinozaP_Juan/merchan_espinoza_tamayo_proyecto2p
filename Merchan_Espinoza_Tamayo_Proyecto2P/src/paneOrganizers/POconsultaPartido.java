/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paneOrganizers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import javafx.collections.FXCollections;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import manejoArchivos.ArraysData;
import modelo.Pais;
import modelo.Partido;

/**
 *
 * @author A1
 */
public class POconsultaPartido {

    private VBox root;
    private ComboBox<String> comboEquipo1;
    private ComboBox<String> comboEquipo2;
    private ComboBox<String> faseCB;
    private ComboBox<String> grupoCB;
    private Label eq1;
    private Label eq2;
    private Label vs;
    private Scene sc;
    private TreeMap fasesPartidos;
    private ObservableList<String> localList;
    private ObservableList<String> visitaList;
    private Stage primaryStage;
    private GridPane gridPane;
    private Button consulBt;
    private Button detalleEquipos;
     private VBox resultado;
    private Partido partido;
    

    public POconsultaPartido(Stage primaryStage) {
        this.primaryStage = primaryStage;
        root = new VBox(40);
        root.setPadding(new Insets(30, 15, 30, 15));
        root.setAlignment(Pos.TOP_CENTER);
        Label lb = new Label("Consulta de Partidos");

        gridPane = new GridPane();
        seccionCB();
        consulBt = new Button("Consultar");

        root.getChildren().addAll(lb, gridPane, consulBt);
        sc = new Scene(root, 800, 700);
        
        resultado = new VBox();
        resultado.setAlignment(Pos.CENTER);        
        root.getChildren().add(resultado);
        
        detalleEquipos = new Button("VER DETALLE DE EQUIPO");        
        detalleEquipos.setPadding(new Insets(10, 5, 10, 5));
        
        //manejo boton consultar
        consulBt.setOnAction(e -> {            
            resultado.getChildren().clear();            
            String equipo1 = comboEquipo1.getValue();
            String equipo2 = comboEquipo2.getValue();
            
            int x = 0;
            Label r = new Label("Resultado del partido");
            r.setId("subtitulo");
            resultado.getChildren().add(r);
            for (Partido p : ArraysData.partidos) {
                
                if (p.getPaisLocal().equals(equipo1) && p.getPaisVisita().equals(equipo2)) {
                    crearSeccionconsultar(p);
                    partido = p;
                    x = 1;
                }
            }
            if (x == 0) {
                resultado.getChildren().add(new Label("Los equipos no se enfrentaron en condicion Local vs Visitante"));
            }
        });
    }

    public Scene getSc() {
         try {
            sc.getStylesheets().add(getClass().getResource("/resources/estilo_ch.css").toString());
        } catch (Exception e) {
            System.out.println("No muestra estilos");
        }
        return sc;
    }

    public void setSc(Scene sc) {
        this.sc = sc;
    }

    public VBox getRoot() {
        return root;
    }

    public void seccionCB() {
        gridPane = new GridPane();

        // Label del gridpane
        Label fase = new Label("Fase:");
        Label grupos = new Label("Grupos:");
        grupos.setVisible(false);
        eq1 = new Label("Equipo1:");
        eq1.setVisible(false);
        eq2 = new Label("Equipo2:");
        eq2.setVisible(false);
        vs = new Label("VS");
        vs.setVisible(false);

        comboEquipo1 = new ComboBox<>();
        fasesPartidos = ArraysData.fasesPartidos;


        comboEquipo1.setVisible(false);
        comboEquipo2 = new ComboBox<>();
        comboEquipo2.setVisible(false);
        faseCB = new ComboBox<>();
        faseCB.getItems().addAll("Grupo", "Ronda de 16", "Cuartos de Final",
                "SemiFinal", "Play off Tercer Lugar", "Final");

        grupoCB = new ComboBox<>();
        grupoCB.getItems().addAll("A", "B", "C", "D", "E", "F", "G", "H");
        grupoCB.setVisible(false);

        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(50);
        gridPane.setHgap(50);
        gridPane.add(fase, 0, 0);
        gridPane.add(faseCB, 1, 0);
        gridPane.add(grupos, 3, 0);
        gridPane.add(grupoCB, 4, 0);
        gridPane.add(eq1, 0, 1);
        gridPane.add(comboEquipo1, 1, 1);
        gridPane.add(vs, 2, 1);
        gridPane.add(eq2, 3, 1);
        gridPane.add(comboEquipo2, 4, 1);

        //Manejo evento combobox fases
        faseCB.setOnAction((ActionEvent e) -> {

            if (faseCB.getValue().equals("Grupo")) {

                grupos.setVisible(true);
                grupoCB.setVisible(true);

            } else {
                grupoCB.setVisible(false);
                grupos.setVisible(false);

                llenarComboBoxEquipos(faseCB);

            }

        });
        

        //Mnejo evento combo grupos
        grupoCB.setOnAction(e -> {
            //System.out.println(grupoCB.getValue());
            
            llenarComboBoxEquipos(grupoCB);
        });
        

    }

    public void llenarComboBoxEquipos(ComboBox<String> cb) {

        comboEquipo1.setVisible(true);
        comboEquipo2.setVisible(true);
        eq1.setVisible(true);
        eq2.setVisible(true);
        vs.setVisible(true);

        //String select = faseCB.getValue();
        String select = cb.getSelectionModel().getSelectedItem();
        ArrayList<Partido> partidos = (ArrayList) fasesPartidos.get(select);
        TreeSet<String> locales = new TreeSet<>();
        TreeSet<String> visitas = new TreeSet<>();
        for (Partido p : partidos) {
            locales.add(p.getPaisLocal());
            visitas.add(p.getPaisVisita());
        }
        localList = FXCollections.observableArrayList(locales);
        visitaList = FXCollections.observableArrayList(visitas);
        comboEquipo1.setItems(localList);
        comboEquipo2.setItems(visitaList);
    }
    
    
    public void crearSeccionconsultar(Partido p) {
        
        VBox datos = new VBox();
        
        Label l2 = new Label(p.getFase().toUpperCase());
        Label l3 = new Label(p.getEstadio());
        l3.setPadding(new Insets(0,20,0,0));
        Label l4 = new Label(p.getCiudad());
        datos.getChildren().addAll(l2, l3, l4);

        HBox bandera_pais1 = new HBox();
        bandera_pais1.setSpacing(5);
        
        bandera_pais1.setAlignment(Pos.BASELINE_LEFT);
        bandera_pais1.setPrefHeight(50);
        Image img = new Image(getClass().getResource("/resources/BanderasCopa2014/" + p.getPaisLocal().toUpperCase() + ".png").toString());
        ImageView paisIV = new ImageView(img);
        paisIV.setFitHeight(20);
        paisIV.setFitWidth(22);
        Label paisLabel = new Label(p.getPaisLocal().toUpperCase());
        bandera_pais1.getChildren().addAll(paisIV, paisLabel);

        HBox bandera_pais2 = new HBox();
        bandera_pais2.setSpacing(5);
        bandera_pais2.setAlignment(Pos.BASELINE_LEFT);
        bandera_pais2.setPrefHeight(30);
       
        Image img2 = new Image(getClass().getResource("/resources/BanderasCopa2014/" + p.getPaisVisita().toUpperCase() + ".png").toString());
        ImageView paisIV2 = new ImageView(img2);
        paisIV2.setFitHeight(20);
        paisIV2.setFitWidth(22);
        
        Label paisLabel2 = new Label(p.getPaisVisita().toUpperCase());
        bandera_pais2.getChildren().addAll(paisIV2, paisLabel2);

        VBox result = new VBox();
        result.setSpacing(8);
        Label l5 = new Label("FINAL DEL PARTIDO");
        l5.setPrefWidth(60);
        Label res = new Label(p.getGoalsLocal() + " - " + p.getGoalsVisita());
        result.getChildren().addAll(l5, res);
        HBox partidoVista = new HBox();
        partidoVista.setSpacing(45);
        partidoVista.getChildren().addAll(datos, bandera_pais1);
        partidoVista.getChildren().add(res);
        partidoVista.getChildren().add(bandera_pais2);
        partidoVista.setPadding(new Insets(10,5,25,5));
        partidoVista.setAlignment(Pos.CENTER);
        VBox general = new VBox();
        general.setAlignment(Pos.CENTER);
        Label l1 = new Label(p.getFechaHora() + " Hora Local");
        Line linea = new Line();
        
        linea.setEndX(500);
        general.getChildren().addAll(l1, linea, partidoVista, detalleEquipos);
        
        
        detalleEquipos.setOnAction(e ->{
           /* 
        for (Partido partido : ArraysData.partidos){
           
            if (partido.getPaisLocal().equals(comboEquipo1.getValue()) &&
                    p.getPaisVisita().equals(comboEquipo2.getValue())){
                this.partido = partido;
            }
        }*/
            POdetalleEquipo poe = new POdetalleEquipo(partido);
            
            Stage stage2 = new Stage();
            stage2.setTitle("Detalle de Equipos");
            Scene sc = new Scene(poe.getRoot(),900,800);
            stage2.setScene(sc);
            stage2.setResizable(true);
            stage2.toFront();
            stage2.show();
            
            System.out.println(partido);
        });
        
        resultado.getChildren().add(general);

    }
}
