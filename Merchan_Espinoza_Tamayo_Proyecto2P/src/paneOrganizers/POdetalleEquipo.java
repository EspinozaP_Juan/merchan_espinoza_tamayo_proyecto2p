package paneOrganizers;

import com.sun.javafx.font.t2k.T2KFactory;
import java.util.ArrayList;
import java.util.Random;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import manejoArchivos.ArraysData;
import modelo.Jugador;
import modelo.Partido;

/**
 *
 * @author Juan Espinoza
 */
public class POdetalleEquipo {

    private VBox root;
    private Label titulo;
    private Label local;
    private Label visita;
    private TilePane paisLocal;
    private TilePane paisVisitante;
    private Partido partido;
    private ImageView iv;
    

    public POdetalleEquipo(Partido partido) {
        this.partido = partido;
        root = new VBox();
        root.setSpacing(25);
        root.setPadding(new Insets(20, 30, 20, 30));
        root.setAlignment(Pos.CENTER);
        titulo = new Label("Detalle de Equipos");
        local = new Label(partido.getPaisLocal());
        local.setAlignment(Pos.CENTER_LEFT);
        visita = new Label(partido.getPaisVisita());
        visita.setAlignment(Pos.CENTER_LEFT);
        paisLocal = crearTilePane();
        paisVisitante = crearTilePane();
        mostrarJugadores();
        root.getChildren().addAll(titulo,local,paisLocal,visita,paisVisitante);

    }

    public TilePane crearTilePane() {

        TilePane tilePane = new TilePane();
        tilePane.setAlignment(Pos.CENTER);
        tilePane.setPadding(new Insets(15, 30, 15, 30));
        tilePane.setHgap(20);
        return tilePane;
    }

    /*
    public ScrollPane crearScrollPane() {
        ScrollPane sp = new ScrollPane();
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); //Vert scrollbar
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);//Horizontal
        sp.setFitToWidth(true);
        return sp;
    }*/
    public void mostrarJugadores() {
        TreeSet<Jugador> jj = new TreeSet<Jugador>(ArraysData.jugadores);
        // && j.getMatchID().equals(partido.getMatchID())
        for (Jugador j : jj) {
            if (j.getTeamInitials().equals(partido.getInicialLocal())) {
                paisLocal.getChildren().add(crearVistaJugador(j));
                cambiarImagenJugador(j);

            }if (j.getTeamInitials().equals(partido.getInicialVisita())) {
                paisVisitante.getChildren().add(crearVistaJugador(j));
                cambiarImagenJugador(j);
            }
        }
    }

    public void cambiarImagenJugador(Jugador j) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Random r = new Random();
                int segundos = r.nextInt(11) + 5;
                try {
                    Thread.sleep(segundos*1000);
                    
                } catch (InterruptedException ex) {
                    Logger.getLogger(POdetalleEquipo.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (j.getTeamInitials().equals("BRA")){
                    //j.setImg(new Image(getClass().getResource("/resourses/PlayersPhoto/Brasil/"+j.getShirtNum()).toString()));
                    iv.setImage(new Image(getClass().getResource("/resourses/PlayersPhoto/Brasil/"+j.getShirtNum()).toString()));
                }
                else if (j.getTeamInitials().equals("MEX")){
                    //j.setImg(new Image(getClass().getResource("/resourses/PlayersPhoto/Mexico/"+j.getShirtNum()).toString()));
                    iv.setImage(new Image(getClass().getResource("/resourses/PlayersPhoto/Mexico/"+j.getShirtNum()).toString()));
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }


    public VBox crearVistaJugador(Jugador jugador) {

        VBox vbox = new VBox(8);
        vbox.setPadding(new Insets(5));

        iv = new ImageView(jugador.getImg());
        iv.setPreserveRatio(true);
        iv.setFitWidth(50);
        Label nombre = new Label(jugador.getNombre());
        nombre.setMaxWidth(50);
        nombre.setFont(Font.font(12));

        vbox.getChildren().addAll(iv, nombre);

     
        //Manejo de evento de click sobre Imagen de jugador
        iv.setOnMouseClicked(e -> {
            Stage st = new Stage();
            POdetalleJugador poj = new POdetalleJugador(jugador);
            Scene scena = new Scene(poj.getRoot(), 200, 300);
            st.setScene(scena);
            st.show();
            Thread t = new Thread(new Runnable() {
                @Override
        public void run() {
                    runTask(poj.getMensaje(), st);
                }
            });
            t.setDaemon(true);
            t.start();
        });
        return vbox;

    }

    public void runTask(Label mensaje, Stage detalleJugadores) {
        for (int i = 10; i >= 0; i--) {
            int segundo = i;
            try {
                System.out.println(Thread.currentThread());
                String message = "Mostrando por " + i + " segundos";
                // Mensaje a mostrar en el Label

                Platform.runLater(new Runnable() {
                    public void run() {
//                        System.out.println(Thread.currentThread());
                        mensaje.setText(message);
                        //Sebe cerrar el hilo y la ventana cuando i llegue a cero
                        if (segundo == 0) {
                            detalleJugadores.close();
                            Thread.currentThread().interrupt();
                        }
                    }
                });
                //Lambda Expression
                //Platform.runLater(() -> statusLbl.setText(status));
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public VBox getRoot() {
        return root;
    }

}
