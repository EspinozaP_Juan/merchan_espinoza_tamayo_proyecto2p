package paneOrganizers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import manejoArchivos.ArraysData;
//import manejoArchivos.ArraysData;

/**
 *
 * @author A1
 */
public class Principal {

       private VBox seccionPrincipal;
//    private HBox principal;
    private Stage primaryStage;
    private Scene sc;
    private Button btconsultaCopa;
    private Button btconsultaPartido;

    public Principal(Stage primaryStage) {
        seccionPrincipal = new VBox(40);
        this.primaryStage = primaryStage;
        try {
            CrearSeccionPrincipal();
        } catch (IOException | RuntimeException ex) {
            seccionPrincipal.getChildren().clear();
            seccionPrincipal.getChildren().add(new Label("Ha ocurrido un error con la pagina"));
            seccionPrincipal.getChildren().add(new Label(ex.getMessage()));

        }
        sc = new Scene(seccionPrincipal,800,650);
    }
    
    public VBox getPrincipal() {
        return seccionPrincipal;
    }

    public void CrearSeccionPrincipal() throws IOException {

        // crear imagen de la pagina principal
        Image imagenPrincipal = new Image(getClass().getResource("/resources/Fondo.jpg").toString(), 650, 350, false, false);
        ImageView IVimagenPrincipal = new ImageView(imagenPrincipal);

        // agg nodos a la seccion principal
        seccionPrincipal.getChildren().addAll(TituloPPrincipal(), IVimagenPrincipal, BotonesPPrincipal());
        seccionPrincipal.setAlignment(Pos.CENTER);
//        seccionPrincipal.setStyle("-fx-background-color:WHITE");

    }
    
    // CREAR TITULO PAGINA PRINCIPAL
    
    public HBox TituloPPrincipal(){
        HBox titulo = new HBox(20);
        Label lbTitulo = new Label("Copa Mundial de la FIFA Brasil 2014");
        lbTitulo.setStyle("-fx-font-size:2.5em;-fx-font-weight:bolder");
        Image iconoTitulo = new Image(getClass().getResource("/resources/MascotaMundial.jpg").toString(), 85, 95, false, false);
        ImageView IViconoTitulo = new ImageView(iconoTitulo);
        titulo.getChildren().addAll(lbTitulo, IViconoTitulo);
        titulo.setAlignment(Pos.CENTER);
         return titulo;
    }
    
    // CREAR BOTONES DE LA PAGINA PRINCIPAL

    public HBox BotonesPPrincipal() {

        HBox botonesPrincipal = new HBox(20);
        btconsultaPartido = new Button("CONSULTA DE PARTIDOS");
//        btconsultaPartido.setStyle("-fx-base:blue");
        
        btconsultaCopa = new Button("CONSULTA DE COPAS MUNDIALES");
//        btconsultaCopa.setStyle("-fx-base:blue");


        botonesPrincipal.getChildren().addAll(btconsultaPartido, btconsultaCopa);
        botonesPrincipal.setAlignment(Pos.CENTER);
        
        return botonesPrincipal;
    }

    public Scene getScene() {
        
        // Evento para cambiar de scene
        
        EventHandler<ActionEvent> s = new EventHandler() {        
            @Override
            
            public void handle(Event event) {
                Scene scene2 = new POconsultaHistorica(ArraysData.copasMundiales,primaryStage).getScene();
                primaryStage.setScene(scene2);
            }
        };
        btconsultaCopa.setOnAction(s);
        
        btconsultaPartido.setOnAction(Event->{
            Scene scene3= new POconsultaPartido(primaryStage).getSc();
            primaryStage.setScene(scene3);
            
        });
        try {
            sc.getStylesheets().add(getClass().getResource("/resources/estilo.css").toString());
        } catch (Exception e) {
            System.out.println("No muestra estilos");
        }
        
        return this.sc;
        
    }

}
