package Aplicacion;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;
import manejoArchivos.ArraysData;
import paneOrganizers.POconsultaHistorica;
import paneOrganizers.Principal;

/**
 *
 * @author Carla
 */
public class AplicacionCopaMundial extends Application {

    @Override
    public void start(Stage primaryStage) {
        
         // Cosnstruccion de ArrayList para todos los objetos
        ArraysData data = new ArraysData();
        Principal p = new Principal(primaryStage);
        primaryStage.setScene(p.getScene());
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
