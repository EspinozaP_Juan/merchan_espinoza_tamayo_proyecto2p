package modelo;

import java.util.Objects;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Carla
 */
public class Jugador implements Comparable<Jugador> {

    private String nombre;
    private int shirtNum;
    private String teamInitials;
    private String coach;
    private String matchID;
    private Image img;

    public Jugador(String nombre, int shirtNum, String teamInitials,
            String coach, String matchID) {
        this.nombre = nombre;
        this.shirtNum = shirtNum;
        this.teamInitials = teamInitials;
        this.coach = coach;
        this.matchID = matchID;
        try {
            this.img = new Image(getClass().getResource(
                    "/resources/PlayersPhotos/no-profile.gif").toString());
            //iv = new ImageView(img);
            
        } catch (RuntimeException e) {
            System.out.println("No se puede asignar imagen");
            this.img = null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jugador other = (Jugador) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public int compareTo(Jugador j) {
        int estado = Integer.compare(this.shirtNum, j.shirtNum);
        if (estado == 0) {
            estado = this.nombre.compareTo(j.nombre);
        }
        return estado;
    }

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(String matchID) {
        this.matchID = matchID;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTeamInitials() {
        return teamInitials;
    }

    public int getShirtNum() {
        return shirtNum;
    }

    public String getCoach() {
        return coach;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Jugador{" + "nombre=" + nombre + ", shirtNum=" + shirtNum
                + ", teamInitials=" + teamInitials + ", coach=" + coach
                + ", matchID=" + matchID + '}' + "\n";
    }

}
