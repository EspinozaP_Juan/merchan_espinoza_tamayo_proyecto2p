package modelo;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Carla
 */
public class Pais {

    private String pais;
    private String iniciales;
    private int copas;
    private ArrayList<String> fases;

    public Pais(String pais) {
        this.pais = pais;
        fases = new ArrayList<>();
        copas = 0;
    }

    public ArrayList<String> getFases() {
        return fases;
    }

    public void setFases(ArrayList<String> fases) {
        this.fases = fases;
    }

    public int getCopas() {
        return copas;
    }

    public void ganaCopa() {
        copas++;
    }

    public String getPais() {
        return pais;
    }

    public void setIniciales(String iniciales) {
        this.iniciales = iniciales;
    }

    @Override
    public String toString() {
        return "Pais{" + "iniciales=" + iniciales + ", copas=" 
                + copas + ", pais=" + pais + '}'+"\n";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pais other = (Pais) obj;
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        return true;
    }
}
