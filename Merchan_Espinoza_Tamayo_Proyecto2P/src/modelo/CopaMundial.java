package modelo;

/**
 *
 * @author Carla
 */
public class CopaMundial {

    private int year;
    private Pais first;
    private Pais second;
    private Pais third;
    private Pais fourth;
    private int goalsTotal;
    private int teams;
    private int matches;
    private String attendance;

    public CopaMundial(int year, Pais first, Pais second, Pais third, Pais fourth, int goalsTotal, int teams, int matches, String attendance) {
        this.year = year;
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
        this.goalsTotal = goalsTotal;
        this.teams = teams;
        this.matches = matches;
        this.attendance = attendance;
    }

    public int getGoalsTotal() {
        return goalsTotal;
    }

    public int getTeams() {
        return teams;
    }

    public int getMatches() {
        return matches;
    }

    public String getAttendance() {
        return attendance;
    }

    public int getYear() {
        return year;
    }

    public Pais getFirst() {
        return first;
    }

    public Pais getSecond() {
        return second;
    }

    public Pais getThird() {
        return third;
    }

    public Pais getFourth() {
        return fourth;
    }

    @Override
    public String toString() {
        return "CopaMundial{" + "year=" + year + ", first=" + first 
                + ", second=" + second + ", third=" + third + ", fourth=" 
                + fourth + ", goalsTotal=" + goalsTotal + ", teams=" + teams 
                + ", matches=" + matches + ", attendance=" +attendance+'}'+"\n";
    }
    

}
