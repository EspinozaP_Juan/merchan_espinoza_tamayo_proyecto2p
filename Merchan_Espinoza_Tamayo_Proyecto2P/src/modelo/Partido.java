package modelo;

import java.util.Objects;

/**
 *
 * @author Juan Espinoza
 */
public class Partido implements Comparable<Partido> {
    private String year;
    private String fechaHora;
    private String fase;
    private String estadio;
    private String ciudad;
    private String paisLocal;
    private String goalsLocal;
    private String goalsVisita;
    private String paisVisita;
    private String asistencia;
    private String matchID;
    private String inicialLocal;
    private String inicialVisita;

    public Partido(String year, String fechaHora, String fase, String estadio,
            String ciudad, String paisLocal, String goalsLocal,
            String goalsVisita, String paisVisita, String asistencia,
            String matchID, String inicialLocal, String inicailVisita) {
        this.year = year;
        this.fechaHora = fechaHora;
        this.fase = fase;
        this.estadio = estadio;
        this.ciudad = ciudad;
        this.paisLocal = paisLocal;
        this.goalsLocal = goalsLocal;
        this.goalsVisita = goalsVisita;
        this.paisVisita = paisVisita;
        this.asistencia = asistencia;
        this.matchID = matchID;
        this.inicialLocal = inicialLocal;
        this.inicialVisita = inicailVisita;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.year);
        hash = 59 * hash + Objects.hashCode(this.fechaHora);
        hash = 59 * hash + Objects.hashCode(this.fase);
        hash = 59 * hash + Objects.hashCode(this.estadio);
        hash = 59 * hash + Objects.hashCode(this.ciudad);
        hash = 59 * hash + Objects.hashCode(this.paisLocal);
        hash = 59 * hash + Objects.hashCode(this.goalsLocal);
        hash = 59 * hash + Objects.hashCode(this.goalsVisita);
        hash = 59 * hash + Objects.hashCode(this.paisVisita);
        hash = 59 * hash + Objects.hashCode(this.asistencia);
        hash = 59 * hash + Objects.hashCode(this.matchID);
        hash = 59 * hash + Objects.hashCode(this.inicialLocal);
        hash = 59 * hash + Objects.hashCode(this.inicialVisita);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Partido other = (Partido) obj;
        if (!Objects.equals(this.year, other.year)) {
            return false;
        }
        if (!Objects.equals(this.fechaHora, other.fechaHora)) {
            return false;
        }
        if (!Objects.equals(this.fase, other.fase)) {
            return false;
        }
        if (!Objects.equals(this.estadio, other.estadio)) {
            return false;
        }
        if (!Objects.equals(this.ciudad, other.ciudad)) {
            return false;
        }
        if (!Objects.equals(this.paisLocal, other.paisLocal)) {
            return false;
        }
        if (!Objects.equals(this.goalsLocal, other.goalsLocal)) {
            return false;
        }
        if (!Objects.equals(this.goalsVisita, other.goalsVisita)) {
            return false;
        }
        if (!Objects.equals(this.paisVisita, other.paisVisita)) {
            return false;
        }
        if (!Objects.equals(this.asistencia, other.asistencia)) {
            return false;
        }
        if (!Objects.equals(this.matchID, other.matchID)) {
            return false;
        }
        if (!Objects.equals(this.inicialLocal, other.inicialLocal)) {
            return false;
        }
        if (!Objects.equals(this.inicialVisita, other.inicialVisita)) {
            return false;
        }
        return true;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPaisLocal() {
        return paisLocal;
    }

    public void setPaisLocal(String paisLocal) {
        this.paisLocal = paisLocal;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public String getInicialLocal() {
        return inicialLocal;
    }

    public void setInicialLocal(String inicialLocal) {
        this.inicialLocal = inicialLocal;
    }

    public String getInicialVisita() {
        return inicialVisita;
    }

    public void setInicialVisita(String inicialVisita) {
        this.inicialVisita = inicialVisita;
    }

    public String getGoalsLocal() {
        return goalsLocal;
    }

    public void setGoalsLocal(String goalsLocal) {
        this.goalsLocal = goalsLocal;
    }

    public String getGoalsVisita() {
        return goalsVisita;
    }

    public void setGoalsVisita(String goalsVisita) {
        this.goalsVisita = goalsVisita;
    }

    public String getPaisVisita() {
        return paisVisita;
    }

    public void setPaisVisita(String paisVisita) {
        this.paisVisita = paisVisita;
    }

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(String matchID) {
        this.matchID = matchID;
    }

    @Override
    public String toString() {
        return "Partido{" + "year=" + year + ", fechaHora=" + fechaHora 
                + ", fase=" + fase + ", estadio=" + estadio + ", ciudad=" 
                + ciudad + ", paisLocal=" + paisLocal + ", goalsLocal=" 
                + goalsLocal + ", goalsVisita=" + goalsVisita 
                + ", paisVisita=" + paisVisita + ", asistencia=" 
                + asistencia + ", matchID=" + matchID + ", inicialLocal=" 
                + inicialLocal + ", inicailVisita=" + inicialVisita + '}'+"\n";
    }

    @Override
    public int compareTo(Partido o) {
        int estado = this.paisLocal.compareTo(o.paisLocal);
        if (estado == 0){
            estado = this.paisVisita.compareTo(o.paisVisita);
        }
        return estado;
    }
}
